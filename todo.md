
```
function convertToPlain(rtf) {
  rtf = rtf.replace(/\\par[d]?/g, "");
  rtf = rtf.replace(/\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?/g, "")
  return rtf.replace(/\\'[0-9a-zA-Z]{2}/g, s => {
    return String.fromCharCode(parseInt(''+s.substring(2),16))
  }).trim();
}
```


(template créer un pouvoir)

GENERAL
- changer le nom des applis hepïk-

-- PDF --
1) maquette simple
2) Mise en page => CSS imprimable ? > svg
3) Choisir les pouvoirs à imprimer
4) la description technique ne sera pas dans la fiche perso
5) objectif recto-verso
6) option ajouter emplacement portrait

-- Site --
1) Menu :
   @ login
   @ notes de campagnes
   @ fiches persos (sous-menu)
   @ racines
   @ équipement
   @ lien minas map
2) Recherche de racine: matcher aussi la description
3) Faire l'équivalent des racines pour l'équipement
4) Parser racines/équipement: Markdown => html, requêter l'html
5) Marquer version pdf de capacités parsé