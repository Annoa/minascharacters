#!/bin/bash

git init
mkdir ~/.ssh 
touch ~/.ssh/id_dsa
echo "Host gitlab.com
  StrictHostKeyChecking no" > ~/.ssh/config
echo "$GIT_PRIVATE_KEY" > ~/.ssh/id_dsa
git remote add origin git@gitlab.com:Annoa/minascharacters.git
git fetch
git reset origin/master

SHEET_ID=$1
git config --local user.email "aramile@hotmail.fr"
git config --local user.name "Treestime"
git add ./server/data/sheets/$1.md
git commit -m "New data ($1)"
git push origin master