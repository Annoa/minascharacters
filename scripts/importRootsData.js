// https://stackoverflow.com/questions/20754127/how-to-convert-a-cp949-rtf-to-a-utf-8-encoded-rtf

const fs = require('fs')
const readline = require('readline')

const FILE = __dirname+'/../server/data/Les_capacites_1.7.6.md'
const OUTPUT = __dirname+'/../server/data/racines.json'

function readAndAddTag(obj, line) {
  clearnedLine = line.replace(/<\/?[^>]+(>|$)/g, "")
  if (['M', 'U', 'R', 'C'].includes(clearnedLine)) {
    obj.rarete = clearnedLine
  } else if (clearnedLine.startsWith('Endurance')) {
    obj.endurance = clearnedLine.split(':')?.[1] || '0'
  } else if (clearnedLine.startsWith('Mana')) {
    obj.mana = clearnedLine.split(':')?.[1] || '0'
  } else if (clearnedLine.startsWith('Volonté')) {
    obj.volonte = clearnedLine.split(':')?.[1] || '0'
  } else if (clearnedLine === 'Factorisable') {
    obj.factorisable = true
  } else if (clearnedLine === 'Instantané') {
    obj.instant = true
  } else if (clearnedLine === 'Cumulatif') {
    obj.cumulative = true
  } else if (clearnedLine === 'Instantané') {
    obj.other = obj.other?obj.other+' ':''+clearnedLine
  } 
}


function lineToArray(line) {
  return line.split('|').map(s => s.trim()).filter(e => e)
}

function descToHtml(desc) {
  if (!desc) return undefined
  let res = '<div>'
  let inTable = false, th = false
  for (const line of desc) {
    if (line.startsWith('|:')) {
      continue
    }
    if (line.startsWith('| ')) {
      if (!inTable) {
        inTable = true
        th = true
        res += '<table><thead>'
      } 
      if (th) {
        res += `<tr>${
          lineToArray(line).map(s => `<th>${s}</th>`).join('')
        }</tr><thead><tbody>`
        th = false
      } else {
        res += `<tr>${
          lineToArray(line).map(s => `<td>${s.replace(/\*.*?\*/g, s=>`<i>${s.split('*').join('')}</i>`)}</td>`).join('')
        }</tr>`
      } 
    } else {
      if (inTable) {
        res += '</tbody></table>'
        inTable = false
      }
      if (!line.trim().length) {
        console.log("IL Y A DES LIGNES VIDES !!!")
      }
      if (!line.startsWith('```')) {
        if (line.match(/<div .*>/) || line === '</div>') {
          res += line
        } else {
          res += `<p>${line.replace(/\*.*?\*/g, s=>`<i>${s.split('*').join('')}</i>`)}</p>`
        }
      }
    }
  }

  if (inTable) {
    res += '</tbody></table>'
  }

  return res + '</div>'
}

async function processRacinesDesc() {
  const rl = readline.createInterface({
    input: fs.createReadStream(FILE),
    crlfDelay: Infinity
  })
  const res = {}
  let name = ''
  let subName = ''
  let type = ''
  ignoreLines = false
  for await (const line_ of rl) {
    if (
      line_.startsWith("<div class='pageNumber'") || 
      line_.startsWith("<div class='footnote'")
    ) {
      continue
    }
    if (ignoreLines && line_.startsWith('</div>')) {
      ignoreLines = false
    } else if (line_.startsWith('<div class=\'wide\'>')) {
      ignoreLines = true
    }
    
    const line = line_.trim().replace(/<img\/?[^>]+(>|$)/g, "")
    if (
      !line ||
      line.startsWith('\\')
    ) {
      continue
    }
    if (line === '## Les branches génériques') {
      type = 'generique'
    } else if (line.startsWith('## Les racines')) {
      type = 'normal'
    }  
    else if (line.startsWith('## ')) {
      type = ''
    }
    if (!type) {
      continue
    }
    if (ignoreLines) {
      continue
    }
    // RACINE
    if (line.startsWith('### ') || (type === 'generique' && line.startsWith('#### '))) {
      name = line.substring(4)
      subName = ''
      res[name] = {desc: []}
      if (type === 'generique') {
        res[name].generique = true
      }
    } 
    // Racines tags
    else if (!subName && line_.startsWith('<inline style')) {
      readAndAddTag(res[name], line)
    }
    
    // BRANCHE
    else if (name && line.startsWith('#### ')) {
      subName = line.substring(5)
      res[name][subName] = {desc: []}
    } 
    // Branchs tags
    else if (name && line_.startsWith('<inline style')) {
      readAndAddTag(res[name][subName], line)
    }

    // push in resultat
    else if (subName) {
      res[name][subName].desc.push(line)
    }
    else if (name) {
      res[name].desc.push(line)
    }
  }
  return res
}

function resultToArray(obj) {
  const array = []
  let i = 0
  for (const [key, value] of Object.entries(obj)) {
    const obj = {id:i, name: key, branch: []}
    for (const [key2, value2] of Object.entries(value)) {
      if ([
        'generique',
        'rarete',
        'endurance',
        'mana',
        'volonte',
        'factorisable',
        'factorisable',
        'cumulative',
        'other'
      ].includes(key2)) {
        obj[key2] = value2
      } else if (key2 === 'desc') {
        obj[key2] = descToHtml(value2)
      } else {
        let branchDesc = descToHtml(value2.desc)
        obj.branch.push({
          name: key2,
          ...value2,
          desc: branchDesc
        })
      }
    }
    array.push(obj)
    i++
  }
  return array
}

async function main() {
  const obj = await processRacinesDesc()
  const array = resultToArray(obj)
  const json = JSON.stringify(array)
  fs.writeFileSync(OUTPUT, json)
}

main()