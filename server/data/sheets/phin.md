# Phin

## Général

- Emotion dominante : confiance
- Attitude : Enfant
- Nature : Battant
- PV : 12
- Vitalité : 1
- Aplomb : 1

## Xp

- XP (générique) : 1/84
- XP (apprentissage) : 3/60
- XP (péril) : 0/2
- PC : 2
- Gain d'expérience primaire : Apprentissage
- Gain d'expérience secondaire : Péril

## Ressources

- Endurance : 10/10 [12]
- Mana : 22/22 [27]
- Volonté : 10/10 [10]

## Racines connues

Déplacement (C), Enveloppe (C), Guérison naturelle (C), Sens physique (C), Attaque physique (C), Lumière (C), Sens Magique (C), Force (C), Son (C), Prime (U), Conduit (U), Contre-magie (U), Antidétection (U).
Total : 17 XP

## Compétences

- Langue (Quenya) : 4
- Langue (Thoan) : 1
- Acrobaties : 3
- Bagarre : 1
- Détection : 2
- Discrétion : 3
- Fouille : 2
- Escalade : 2
- Science (Bricollage) : 3
- Connaissance (Minass-Yasse) : 2
- Connaissance (Argot Minass-Yasse) : 2
- Connaissance (Magie) : 3
- Droit : 2
- Bluff : 2
- Do : 1
Total : 33 XP

## Dons

Magie maîtrisé (6PX), Génie (6 XP), Création de Charmes (2 XP), Fiole d'endiguement sonique x10 (1 PX)

## Traits uniques

Adaptabilité Humaine (15PX)

## Capacités

### Corps d'enfant

L'enfant humain à 12 PVs, peut porter 10kg et se déplace à 9m/round aussi bien sur la terre que dans l'eau. Il regagne naturellement 1 PV par jour (avec repos). Cet effet réserve 2 pts d'Endurance.

- Enveloppe (Endurance)
 - Points de vie : 7pc > 12pv
 - Charge transportable : 2pc > 20kg
- Déplacement (Endurance)
 - Type : 1pc > Terrestre
 - Vitesse : 4pc > 9m/round
- Guérison naturelle (Endurance)
 - Efficacité : 2pc > 1/jour
- Branches factorisables
 - Permanence : 50% > permanente
Total : 24pc

### Vision Humaine

Phin a bénéficié de potions d'Empelos afin que ses yeux puissent percevoir la magie. Cet effet réserve 1 pt de Mana.

- Sens physique (Endurance)
 - Sens : 1pc > sens basique
- Sens Magique (Mana)
 - Type : 3pc > Vision magique
- Branche factorisables
 - Permanence : 50% > permanente
 - Portée : 4pc > 9m
Total : 12pc

### Polyphonie

Permet a Phin de parler a deux endroits a la fois (dans un rayon de 18m). Cet effet réserve 1 pt de Mana.

- Son (Mana)
 - Création : 2pc > 4m3
- Branche factorisables
 - Permanence : 50% > permanente
 - Portée : 3pc > 18m
Total : 8pc

### Force mineur
Phin peut dépenser 1 de Mana pour être capable pendant 3 rounds de déplacer 5 kg de matière dans un rayon de 18m. Il bénéficie également d'un bouclier de 5 (Force). Ce sort réserve 1 pt de Mana.

- Force (Mana)
 - Controle : 4pc > 5kg
 - Bouclier : 2pc > 5
- Branche factorisables
 - Activation : 0pc > 1 action
 - Permanence : 30% > activation
 - Concentration : -15%
 - Portée : 3pc > 18m
 - Durée : 4pc > 3 rounds
Total : 14pc

### Sphère de déflexion
Phin est capable de générer une sphère de déflexion autour de lui, de taille variable permettant de rendre invisible toute les personnes à l'intérieur de la sphère, atténuant même les sons provenant de la sphère. La sphère de Déflexion coute 3 Mana par heure d'activation. Ce sort réserve 2 pts de Mana.

- Lumière (Mana)
 - Déflexion : 6pc > 100.%
- Son (Mana)
 - Moduler : 2pc > réduit de 50.% le volume sonore
- Antidétection (Mana)
 - Efficacité : 8pc > 75.% 
- Branche factorisables
 - Durée : 15pc > 1 heure
 - Zone d'effet : 2pc > 3m
 - Composant : -1pc > Gestuel
 - Composant : -1pc > Verbal
 - Activation : 0pc > 1 action
 - Complication : -2%
 - Permanence : 30% > activation
 - Concentration : -15%
Total : 34pc

### Petit coup de poing
L'enfant donne un coup de poing. Il inflige 1 dégâts (contondant) au prix d'une action gestuelle.

- Attaque physique *(Endurance)*
 - Dégâts [Contondant] : 1pc > 1 dégâts
- Branche factorisables
 - Cible : 0pc > 1 cible
 - Activation : 0pc > 1 action
 - Durée : 0pc > instantanée
 - Type : 0pc > Corps à corps
 - Composant : -1pc > gestuelle
Total : 0pc

### Feuille morte
Phin est capable d'amortir sa chute. Il ne prends pas de dégats de chute si jamais le sort fait toujours effet quand il touche le sol. Une plume de Griffon perd son essence lors d'incantation. Phin doit se trouver en chute libre pour pouvoir activer ce sort. Il y a 100% de chance pour que l'efficacité soit réduit de 50%. Le coût du sort est de 8 (x0.85) = 7 Mana.

- Force (Mana)
 - Controle : 6pc > 100kg (50kg avec la complication)
- Branche factorisables
 - Portée : 0pc > personnel
 - Durée : 0pc > 1 round
 - Activation : 15pc > action réflexe
 - Condition d'activation : -4pc > chute libre
 - Composant : -2pc > matériel (plume de Griffon)
 - Composant : -1pc > verbal
 - Composant : -1pc > focalisateur
 - Complication : -24%
 - Concentration : -15%
Total : 8pc

### Contrefeu de duel par Narwe
Ce contresort est efficace contre deux racines de Magie (ici Feu et Foudre). Pour que le contresort fonctionne il faut que le lanceur ait reconnu l'incantation (jet de connaissance de la Magie seulement si composante verbale et même langue, désavantagé). Il y a 10% de chance pour que le lanceur perde 1 PV. Le cout du sort est de 13 (x0.85) = 11 Mana.

- Contre-magie (Mana)
 - Efficacité : 2pc > 50.%
 - Cible : 6pc > 2 racines [Feu, Foudre]
- Branche factorisables
 - Portée : 0pc > 0,5m
 - Durée : 0pc > instantanné
 - Activation : 15pc > action réflexe
 - Condition d'activation : -6pc > Jet de Connaissance (Magie) avec désavantage + sort avec composant verbale
 - Composant : -1pc > gestuel
 - Composant : -1pc > verbal
 - Composant : -1pc > focalisateur
 - Complication : -1pc > 10.% de 10.% de contrecoup
 
Total : 13pc

### Double controlé

Creait un double illusoir de Phin aux prix d'une minute de gestuel, Phin doit se voir d'une manière ou d'une autre pour lancer ce sort. Phin percoit a travers l'illusion tant qu'il reste dans un rayon d'un kilometre du double (sur concentration).
Phin peut donner des ordres simples au double. Phin peut faire apparaitre des accessoires illusoires au double. Il y a 10% de chance qu'il y ait 25% de chance que les observateurs se rendent compte de l'illusion. L'enfant humain peut mettre fin a ce sort a n'importe quel moment. Phin peut parler a travers le double et le double produit des sons crédibles.
Le coût du sort est de 14 (x0,85) = 12 Mana. -3 Mana sans le son, -4 Mana sans l'augmentation d'aura, -5 sans le son et l'augmetation d'aura.

- Lumière (Mana)
 - Illusion : 1pc
- Son (Mana)
 - Création : 1pc > 1m3
- Conduit (Mana)
 - Cible : 3pc
 - Type de lien : 1pc
 - Portée : 1pc
- Antidétection (Mana)
 - Modulation : 2pc > 25.% plus forte 
- Branche factorisables
 - Durée : 15pc > 1 heure
 - Activation : -6pc 
 - Gestuel -1pc 
 - Condition d'activation : -4pc
 - Complication : -2%
 - Polyvalent 10%
Total : 14pc

### Brise-dent

L'enfant lance une caillou. Il inflige 3 dégâts (Force) au prix d'une action gestuelle. 10% de chance pour que le caillou inflige 1 degat (Force).

- Force (Mana)
 - Dégâts [Force] : 2pc > 3 dégâts
 - Cible : 0pc > 1 cible
 - Activation : 0pc > 1 action
 - Durée : 0pc > instantanée
 - Portee : 3pc > 9m
 - Type d'attaque : 1pc > Projectile
- Branche factorisables
 - Composant : -2pc > Matériel
 - Composant : -1pc > Gestuel
 - Composant : -1pc > Verbale
 - Complication : -1pc
Total : 1pc

### Extraction de Mana de famillier 

On extrait jusqu'à 6 mana pour 1 Mana.

- Prime (Mana) 
 - Extraction : 2pc > 6 Mana 
 - Type : 10pc > familier
- Branche factorisables
 - Portée : 0pc > 50cm 
 - Activation : -10pc > 1 heure
 - Composante : -1pc > gestuelle
 - Complication : -1pc
Total : 0pc

## Inventaire

### Fiole d'endiguement sonique x10
Si la fiole est ouverte les sons sont fortement étouffé (75%) dans un rayon de 1,5m. Si la fiole est brisé dans l'heure qui suit, les sons absorbés par la fiole sont violamment libéré, infligeant jusqu'a 25 dégats (Son) dans un rayon de 3m. Pour déterminer les dégats exacte faire un produit en croix sur le nombre de minute ou la fiole a absorbé du son (pour un maximum de 25 en 60 minutes).
- Son (Mana)
 - Moduler : 6pc > 75.%
 - Dégâts (Son) : 10pc > 25 (Son)
- Branche factorisables
 - Durée : 15pc > 1 heure
 - Condition d'activation : -6pc
 - Zone d'effet : 2pc > 3m