# Vyria

## Général

- Emotion dominante : Confiance
- Attitude : Enfant
- Nature: Survivant
- PVs : 22
- Vitalité : 2
- Aplomb : 1

## XP

- Gain d'expérience primaire: Mutation
- Gain d'expérience secondaire: Apprentissage
- XP (générique) : 3/84
- XP (mutation) : 0/2
- XP (apprentissage) : 0/28
- PC : 1

## Ressources

- Endurance : 13/13 [24]
- Mana : 7/7 [8]
- Volonté : 4/4 [4]

## Racines connues
Déplacement (C), Enveloppe (C), Guérison naturelle (C), Sens physique (C), Régénération (U), Attaque physique (C), Changeforme (U), Tenebre (C).
Total : 10 XP

## Compétences

- Langue (Quenya) : 3
- Connaissance (Minass-Yasse) : 3
- Connaissance (Magie) : 3
- Connaissance (Sciences Cosmologique) : 2
- Connaissance (Xeno-biologie) : 2 
- Détection : 3
- Discrétion : 3
- Pistage : 3
- Bagarre : 3
- Do : 3
- Intimidiation : 1
- Fouille : 2
Total : 31 XP

## Traits uniques

Morphose Protéan (20PX)

## Capacités

### Corps Protéan

Vyria a 22 PVs, peut porter 60kg et se déplace à 9m/round aussi bien sur la terre que dans l'eau. Il se régénère des dégâts liés aux ressources au rythme de 1 PV par heure. Il regagne naturellement 1 PV par jour (avec repos). Enfin il bénéficie d'une réduction des dégats de 3 (Tranchant, Contondant, Perforant). Cet effet réserve 8 pts d'Endurance.

- Enveloppe (Endurance)
 - Points de vie : 10pc > 22pv
 - Charge transportable : 6pc > 60kg
 - Résistance interne : 6pc > Réserve de 2 de vitalité
- Déplacement (Endurance)
 - Type : 1pc > Terrestre
 - Type : 2pc > Nage
 - Vitesse : 4pc > 9m/round
- Guérison naturelle (Endurance)
 - Efficacité : 2pc > 1/jour
- Régénération (Endurance)
 - Type de dégât : 6pc > fonctionne sur les dégâts liés a des ressources.
 - Efficacité : 6pc > 1/heure
- Racine Résistance physique (Endurance)
 - Réduction physique (Tranchant) : 4pc > 3 
 - Réduction physique (Contondant) : 4pc > 3
 - Réduction physique (Perforant) : 4pc > 3
- Branches factorisables
 - Permanence : 50%  > Permanente
Total : 83pc

### Vision Protéan

Le très jeune Protéan a les mêmes sens qu'un être humain adulte, mais bénéficie en plus de l'odorat d'un chien et de la vision nocturne d'un chat. Cet effet réserve 1 pt d'Endurance et 1 pt de Mana.
- Sens physique (Endurance)
 - Sens : 1pc > sens basique
 - Sens : 2pc > Odorat
 - Sens : 3pc > Vision nocturne
- Sens Magique (Mana)
 - Type : 3pc > Vision magique
- Branche factorisables
 - Permanence : 50% > Permanente
 - Portée : 4pc > 9m
Total : 20pc

### Changeforme

Demande 3 pts d'Endurance pour activer cette capacité pendant 10 minutes (ou pour la maintenir). La transformation prends 1 round entier. Cet effet réserve 2 pts d'Endurance.

- Changeforme
 - Vitesse : 6pc > 1 round
 - Diminution : 4pc > 50.%
 - Aggrandissement : 2pc > 25 pourcent.%
 - Déguisement : 2pc > Déguisement 3
 - Durée : 12pc > 10 minutes
- Branche factorisables
 - Permanence : 30% > Activation
Total : 34pc
 
### Morsure Protéan

Le Protéan mort sa cible. Il inflige 3 dégâts (tranchant) au prix d'une action gestuelle. Attention : il y a 10 pourcent de chances que le Protéan se blesse et subisse 1 dégât.

- Attaque physique (Endurance)
 - Dégâts [Tranchant] : 2pc > 3 dégâts
 - Cible : 0pc > 1 cible
 - Activation : 0pc > 1 action
 - Durée : 0pc > instantanée
 - Type : 0pc > Corps à corps
 - Composant : -1pc > gestuelle
- Branche factorisables
 - Complication : -1pc (10 pourcent de chance de subir 1 dégat)
Total : 0pc

### Morsure supérieure Protéan

Le Protéan mort sa cible. Il inflige 7 dégâts (tranchant) au prix d'une action gestuelle. Attention : il y a 10% de chances que le Protéan se blesse et subisse 1 dégât.

- Attaque physique (Endurance)
 - Dégâts [Tranchant] : 4pc > 7 dégâts
 - Cible : 0pc > 1 cible
 - Activation : 0pc > 1 action
 - Durée : 0pc > instantanée
 - Type : 0pc > Corps à corps
 - Composant : -1pc > gestuelle
- Branche factorisables
 - Complication : -1pc (10.% de subir 1 dégat)
Total : 2pc

### Camouflage Protéan mineur

Le Protean gagne un camouflage de 50% pendant 1 round.
Attention : il y a 10% de chances que le camouflage soit de 25%.
- Ténèbre (Mana)
 - Dissimulation : 2pc > 50.%
 - Cible : 0pc > 1 cible
 - Activation : 0pc > 1 action
 - Durée : 0pc > 1 round
 - Composant : -1pc > gestuelle
- Branche factorisables
 - Complication : -1pc (10% de chance de subir 1 de réduire de 50% l'efficacite)
Total : 0pc

## Equipement
