# Haurach Onoro

## Général

Émotion dominante : surprise
Attitude : Solitaire
Nature : Visionnaire

PV : 40/40
Vitalité : 1
Aplomb : 1

Repos : 4 h/jour
Régime : Omnivore (et roches)

## XP

Gain d'expérience primaire : Vieillissement
Gain d'expérience secondaire : Apprentissage

- XP (générique) : 1/84
- XP (vieillissement) : 0/3
- XP (apprentissage) : 0/30
- PC : 5

## Ressources

Endurance : 7/7 [20]
Mana : 12/12
        Magenta : 4/4 [4]
	Cyan : 4/4 [4]
	Jaune : 4/4 [4]
Volonté : 3/3 [3]

## Racines

Déplacement (C), Enveloppe (C), Guérison naturelle (C), Sens physiques (C), Résistance physique (C), Résistance énergétique (U), Feu (C), Foudre (C), Froid (C), Force de la nature (U)
Total : 12 XP

## Compétences

- Alchimie : 3
- Artisanat (joaillerie) : 3
- Artisanat (forge) : 3
- Connaissance (Minass-Yassë) : 3
- Connaissance (Religion) : 2
- Connaissance (Magie) : 2
- Détection : 3
- Estimation : 3
- Fouille : 3
- Langue (Quenya) : 4
- Langue (Draconique) : 3
- Psychologie : 1
Total : 33 XP

## Dons

Bichrome (6 XP)

## Traits uniques

Magie Chromatique (15 XP)

## Capacités

### Corps Draconique

Le dragon a 40PV, peut porter 160kg et se déplace à 12m/rnd aussi bien dans les airs que sur la terre et dans l'eau. Sa force lui octroie +4 dégats aux attaques physiques qu'il inflige à l'aide de ses membres. Les dégâts tranchants et perforants sont réduits de 3, et les dégâts de foudre, de feu et de froid sont réduits de 5. Il regagne naturellement 2PV par jour. Cet effet réserve 11 pts d'Endurance.

- Enveloppe (Endurance)
    - Points de vie : 14pc > 40pv
    - Charge transportable : 8pc > 80kg [bonus +75%]
- Déplacement (Endurance)
    - Type : 
        1pc > Terrestre
        2pc > Nage
        6pc > Vol
    - Vitesse : 6pc > 12m/rnd
- Résistance physique (Endurance)
    - Réduction physique [Tranchant] : 4pc > 3
    - Réduction physique [Perforant] : 4pc > 3
- Résistance énergétique (Endurance)
    - Réduction énergétique [Foudre] : 8pc > 5
    - Réduction énergétique [Feu] : 8pc > 5	
    - Réduction énergétique [Froid] : 8pc > 5
- Guérison naturelle (Endurance)
    - Efficacité : 6pc > 2/j
- Force de la nature
    - Force : 10pc > +4 dégâts physiques, +100.% transportable
- Branche factorisable
    - Permanence : 50% > permanente
Total : 114pc

### Vision Draconique
		
Le dragon a les mêmes sens qu'un prédateur (vision de jour comme de nuit et puissant odorat). Cette effet réserve 2 pts d'Endurance.

- Sens physiques (Endurance)
    - Sens : 1pc > Sens basique
    - Sens : 2pc > Odorat
    - Sens : 3pc > vision nocturne
    - Permanence : 50% > permanente
    - Portée : 8pc > 36m
Total : 21pc

### Morsure & Griffure mineure
	
Le dragon mord et griffe sa cible. Il inflige 6 dégâts (3 tranchant et 3 perforant) au prix d'une action gestuelle. Attention : il y a 10% de chances que le dragon se blesse et subisse 1 dégât.

- Attaque Physique (Endurance)
    - Dégâts [Tranchant] : 1pc > 1 dégât [+2 bonus de force]
    - Dégâts [Perforant] : 1pc > 1 dégât [+2 bonus de force]
    - Composant : -1pc > gestuelle
    - Complication : -1pc > 10.% de chance d'avoir 10.% de contrecoup
    - Cible : 0pc > 1 cible
    - Activation : 0pc > 1 action
    - Durée : 0pc > instannée
    - Type : 0pc > Corps à corps
Total : 0pc

### Morsure & Griffure intermédiaire

Le dragon dépense 2 d'endurances pour mordre et griffer sa cible. Il inflige 10 dégâts (5 tranchant et 5 perforant) au prix d'une action gestuelle. Attention : il y a 10% de chances que le dragon se blesse et subisse 1 dégât.

- Attaque Physique (Endurance)
    - Dégâts [Tranchant] : 2pc > 3 dégât [+2 bonus de force]
    - Dégâts [Perforant] : 2pc > 3 dégât [+2 bonus de force]
    - Composant : -1pc > gestuelle
    - Complication : -1pc > 10.% de contrecoup avec 10.% de chance
    - Cible : 0pc > 1 cible
    - Activation : 0pc > 1 action
    - Durée : 0pc > instannée
    - Type : 0pc > Corps à corps
Total : 2pc

### Souffle

Le dragon tire un trait élémentaire qu'il peut maintenir le temps d'1 round contre une cible à 3m. Ce souffle inflige 3 dégâts. Le souffle coûte 3 Chroma dépendant du type de dégât : magenta pour du feu ; cyan pour du froid ; jaune pour de la foudre. Attention : il y a 10% de chances que le trait n'atteigne pas la cible voire cause des dégâts collatéraux. Version combinée : le dragon peut utiliser 2 types de souffles en même temps.

- Feu (Magenta)
    - Dégâts : 2pc > 3
- Froid (Cyan)
    - Dégâts : 2pc > 3
- Foudre (Jaune)
    - Dégâts : 2pc > 3
- Branche factorisable
    - Type d'attaque : 1pc > trait
    - Portée : 2pc > 3m
    - Complication : -1pc > 10.% de chance de toucher la mauvaise cible
    - Durée : 0pc > 1 round
    - Polyvalence : 10%
Total : 9pc

### Main de braises

Le dragon touche sa cible et lui fait le même effets que des braises qui lui tomberaient dessus, infligeant 1 dégât de feu. Il dépense 1 pt de Mana.

- Feu (Magenta)
    - Composant : -1pc > gestuelle
    - Cible : 0pc > 1 cible
    - Activation : 0pc > 1 action
    - Durée : 0pc > instannée
    - Type : 0pc > Corps à corps
    - Dégât : 1pc > 1
Total : 0pc

### Main de givre

Le dragon touche sa cible et lui fait le même effet que si elle avait exposé son épiderme au blizard, lui infligeant 1 dégât de froid. Il dépense 1 pt de Mana.

- Froid (Cyan)
    - Composant : -1pc > gestuelle
    - Cible : 0pc > 1 cible
    - Activation : 0pc > 1 action
    - Durée : 0pc > instannée
    - Type : 0pc > Corps à corps
    - Dégât : 1pc > 1
Total : 0pc

### Main de foudre

Le dragon touche sa cible et lui fait le même effet que si une anguille avait libéré sa décharge à cet endroit précis, lui infligeant 1 dégât de froid. Il dépense 1 pt de Mana.

- Foudre (Jaune)
    - Composant : -1pc > gestuelle
    - Cible : 0pc > 1 cible
    - Activation : 0pc > 1 action
    - Durée : 0pc > instannée
    - Type : 0pc > Corps à corps
    - Dégât : 1pc > 1
Total : 0pc
    
### Extraction de Mana de l'apprenti

Le dragon touche un objet contenant du mana et dépense 1 mana. Il peut ainsi en extraire jusqu'à 3 mana.

- Prime (Mana)
    - Extraction : 1pc > 3 Mana 
    - Cible : 2pc > objet
    - Portée : 0pc > 50cm 
    - Activation : -2pc > 1 round
    - Composante : -1pc > gestuelle
Total : 0pc

## Inventaire

- Gage de citoyenneté
- Certificat apprenti alchimiste
- Boite métallique pleine de liquide et avec une amorce
- Carte de l'hôtel des spendeurs colorée
- Fiole d'endiguement sonique
- Crédits : 1781
- Onces : 2