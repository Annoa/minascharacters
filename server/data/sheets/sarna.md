# Sarna Onóro  

## Général

- Emotion dominante : Joie
- Attitude : Bon vivant
- Nature : Ange gardien
- PV : 22  
- Vitalité : 2
- Aplomb : 1

## XP

- Gain d'expérience primaire : Absorption et Fusion
- Gain d'expérience secondaire : Vieillissement
- XP : 0/83
- PC : 2/80

## Ressources

Endurance : 5/5 [12]
Mana : 7/7 [11]
Volonté : 6/6 [10]

## Racines

Déplacement (C), Enveloppe (C), Guérison naturelle (C), Sens physique (C), Sens psionique (C), Sens magique (C), Télépathie (C), Esprit de groupe (U), Attaque physique (C), Prime (U), Soin (U), Roche (C), Hypercognition (C), Vision lointaine (C), Parade (C), Absorption (R).

## Compétences
- Langue (Quenya) : 3
- Connaissance (Minass Yassë) : 1
- Connaissance (Magie) : 2
- Connaissance (Religion) : 1
- Connaissance (Roche) : 1
- Connaissance (Psi) : 1
- Connaissance (Ténébreuse) : 1
- Connaissance (Cosmologie) : 1
- Do : 1
- Fouille : 1
- Diplomatie : 1

## Traits uniques
Résilience Ixienne (20PX)

## Capacités

### Corps Ixien
Le très jeune Ixien a 22 PVs et 2 Vitalité, peut porter 20kg et se déplace à 9m/round sur terre. Il regagne naturellement 2 PV par jour (avec repos).
- Enveloppe *(Endurance)*
 - Points de vie : 10pc > 22pv
 - Charge transportable : 2pc > 20kg
 - Resistance interne : 6pc > 2 vitalité
- Déplacement *(Endurance)*
 - Type : 1pc > Terrestre
 - Vitesse : 4pc > 9m/round
- Guérison naturelle *(Endurance)*
 - Efficacité : 6pc > 2/jour
- *Branches factorisables*
 - Permanence : 50% > permanent
Total : 44pc

### Esprit Ixien
Sarna a des sens étranges et développés : il voit et entend, mais perçoit également les vibrations, les auras magiques et psioniques et ce sur 18m. Il peut déplacer son capteur sensoriel jusqu'à 72m. Il communique exclusivement par télépathie et réfléchit plus rapidement que la moyenne. 
- Sens physique *(Endurance)*
 - Sens : 1pc > Sens basique
 - Sens : 4pc > Vibrations
- Sens psionique *(Volonté)*
 - Type : 2pc > 6e sens
- Sens magique *(Mana)*
 - Type : 2pc > 6e sens
- Vision lointaine *(Volonté)*
- Télépathie *(Volonté)*
 - Forme : 2pc > parole
 - Message : 1pc > équivalent d'un round de discussion
 - Réponse : 2pc > oui
 - Type : 0pc > Ciblé
- Esprit de groupe *(Volonté*)
 - Efficacité : 2pc > Gain rang compétences / 2
 - Fonctionnement : 2pc > Même espèce
- Hypercognition *(Volonté)*
 - Efficacité : 6pc > Psychologie 4, Bluff 4
- *Branche factorisables*
 - Permanence : 50% > permanent
 - Portée : 5pc > 18m (Sens physiques, Sens psioniques, Sens magiques), 72m (Vision lointaine)
 - Zone d'effet : 4pc > 1km (Esprit de groupe), 72m (Télépathie), 27m (Vision lointaine)
Total : 50pc

### Absorption Mineure
Résultat de l'absorption du crysmal.
- Absorption *(Endurance, Volonté)*
 - Efficacité: 10pc > 5.%
- *Branche factorisables*
 - Cible : 0pc > 1 cible
 - Portée : 0pc : 50cm
 - Permanence : 50% > permanent
Total : 15pc


### Sculpture Tellurique
Un sort démontrant un certain maîtrise de la manipulation de la terre.
- Roche *(Mana)*
 - Type: 0pc > commun
 - Façonnage: 10pc > 2m3
- *Branche factorisables*
 - Activation : 0pc > 1 action
 - Durée : 4pc > 3rounds
 - Portée : 3pc > 18m
 - Composante : -2pc > gestuelle, focalisateur
 - Condition : -4pc > sol minéral
 - Concentration : -15%
 - Permanence : 25% > activation
Total : 12pc

### Soin
Un sort de soin efficace, qui puise dans la vitalité du lanceur pour soigner.
- Soin *(Mana)*
 - Soin: 10pc > 15 PVs
- *Branche factorisables* 
 - Activation : 0pc > 1 action
 - Cible: 2pc > 3 cible
 - Portée : 3pc : 9m
 - Composante : -2pc > gestuelle, focalisateur
 - Risque de complication : -16%pc > 100.%
 - Effet de complication :  ..pc > 10.% de contrecoup
 - Permanence : 25% > activation
Total : 14pc

### Clairvoyance
Une capacité permettant  d'étendre ses sens sur une longue distance pour un court instant.
- Vision lointaine *(Volonté)*
- *Branche factorisables* 
 - Activation : -4pc > 2 rounds
 - Durée: 4pc > 3 rounds
 - Portée : 10pc : 1km
 - Zone d'effet : 4pc > 27m
 - Composante : -1pc > focalisateur
 - Concentration : -15%pc
 - Permanence : 25% > activation
Total : 14pc

### Nouvellle capa
blabla
- Protecteur *Endurance*
 - Chance : 4pc > 50.%
- *Branche factorisables* 
 Activation : 0pc : 1 action
 - Zone d'effet : 4pc > 3m
 - Durée : 4pc : 3 rounds
 - Etat préjudiciable : -3pc > fatigué
 - Concentration : -15%pc
 - Permanence : 25% > activation
Total : 10pc

### Extraction de Mana de l'apprenti 
En peu de temps, même un apprenti peut extraire des quantités raisonnables de Mana d'un réceptacle.
- Prime *(Mana)*
 - Extraction : 2pc > 6 mana
 - Cible: 2pc > 1 objet
 - Portée : 0pc : 50cm
 - Activation : -2pc > 1 round
 - Composante : -2pc > gestuelle, focalisateur
Total : 0pc
 
### Extraction de Mana de famillier
Il faut un beaucoup de temps pour qu'un apprenti puisse récupérer la Mana de son famillier.
- Prime *(Mana)*
 - Extraction : 2pc > 6 mana
 - Cible: 10pc > 1 famillier
 - Portée : 0pc : 50cm
 - Activation : -10pc > 1 heure
 - Composante : -2pc > gestuelle, focalisateur
Total : 0pc
  
### Stockage de Mana de l'apprenti 
Un temps court est suffisant à un apprenti pour stocker des quantités raisonnables de Mana.
- Prime *(Mana)*
 - Stockage : 5pc > 10 mana
 - Cible: 2pc > 1 objet
 - Portée : 0pc : 50cm
 - Activation : -6pc > 1 minute
 - Composante : -2pc > gestuelle, focalisateur
Total : -1pc
 
### Earthshatter
Sarna plaque sur main sur le sol (composante gestuelle) qui explose jusqu'à 9m de là (1 dégât contondant) et se remodèle pour entraver sa victime.
- Attaque physique *(Endurance)*
 - Dégâts [Contondant] : 1pc > 1
 - Type : 3pc > distance (75.%)
- Roche *(Mana)*
 - Type : 0pc > Commun
 - Façonnage : 4pc > 10cm3
- *Branche factorisables*
 - Portée : 3pc : 9m
 - Cible : 0pc > 1 cible
 - Activation : -2pc > 1 round
 - Composante : -2pc > gestuelle, focalisateur
 - Condition : -4pc > sol minéral
Total : 3pc
 
### Earthquake
Sarna frappe le sol du poing (composante gestuelle) et une vibration se répend autour de lui, infligeant 1 dégât (contondant) et étourdissant ceux touchés.
- Attaque physique *(Endurance)*
 - Dégâts [Contondant] : 1pc > 1
 - Type : 3pc > corps à corps (100.%)
 - Zone d'effet [Cercle] : 1pc > 1m de rayon
 - Etourdissement : 4pc > 50.% 
 - Activation : -2pc > 1 round
 - Composante : -1pc > gestuelle
 - Condition : -4pc > sol minéral
Total : 2pc
 
### Ground Slam
Sarna fracasse violemment le sol (composante gestuelle) qui se morcelle pour infliger 3 dégâts (contondant) dans un cône de 4,5 mètres devant lui.
- Attaque physique *(Endurance)*
 - Dégâts [Contondant] : 2pc > 3
 - Type : 3pc > corps à corps (100.%)
 - Zone d'effet [Cône] : 2pc > 4,5m
 - Activation : -2pc > 1 round
 - Composante : -1pc > gestuelle
 - Condition : -4pc > sol minéral 
Total : 0pc
  
### Soin mineur 
Un sort de soin efficace, mais qui n'est pas conçu pour l'urgence des batailles.
- Soin *(Mana)*
 - Soin: 6pc > 9 PVs
 - Cible: 0pc > 1 cible
 - Portée : 0pc : 50cm
 - Activation : -4pc > 2 rounds
 - Composante : -2pc > gestuelle, focalisateur
Total : 0pc

### Création de roche
Un des premiers sorts appris par ceux qui manipulent la roche.
- Roche *(Mana)*
 - Type: 0pc > commun
 - Création: 4pc > 0.01 m3
 - Portée : 0pc : 50cm
 - Activation : -2pc > 1 round
 - Composante : -2pc > gestuelle, focalisateur
Total : 0pc
 

 
 
