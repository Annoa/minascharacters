const fs = require('fs')

const DATA_PATH = global.appRoot + '/server/data/'

console.log('DATA_PATH', DATA_PATH)

const getRoots = (req, res) => {
  console.log('GET /racines', DATA_PATH+'racines.json')
  try {
    const data = fs.readFileSync(DATA_PATH+'racines.json', 'utf8')
    res.json(JSON.parse(data))
  } catch (error) {
    console.error(error)
    res.status(500).json({error})
  }
}

module.exports = { 
  getRoots
}