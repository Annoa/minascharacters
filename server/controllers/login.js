const CryptoJS = require('crypto-js')
const sha256 = require('crypto-js/sha256')

const jwt = require('jsonwebtoken')

const generateAccessToken = (username) => {
  return jwt.sign(username, process.env.JWT_TOKEN, { expiresIn: '60d' });
}

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]
  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.JWT_TOKEN, (err, user) => {
    
    if (err) {
      console.log(err)
      return res.sendStatus(403)
    }

    req.user = user

    next()
  })
}

const login = (req, res) => {
  console.log('POST /login', req.body)
  const password = req.body.password
  if (password === sha256(process.env.PASSWORD).toString(CryptoJS.enc.Base64)) {
    const token = generateAccessToken({ username: 'Admin' });
    res.json(token)
  } else {
    res.sendStatus(401)
  }
}
const tokenIsValid = (req, res) => {
  console.log('GET /tokenIsValid')
  res.sendStatus(200)
}


module.exports = { 
  authenticateToken, 
  login, 
  tokenIsValid 
}