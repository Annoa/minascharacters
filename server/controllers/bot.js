const hljs = require('highlight.js/lib/core')
const jsdom = require("jsdom")
const parser = require('../../src/helpers/parser')
const sheetsJSON = require('../data/sheets.json') //FIXME
const AWS = require('aws-sdk')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const CMD_PREFIX = '!'

const ENTRIES = [
  { title: 'Ressources', key: 'ressource' },
  { title: 'Compétences', key: 'competence' },
  { title: 'Capacités', key: 'capacite' }
]

hljs.registerLanguage('caraclog', parser.default)
const parsedSheets = {}

const updateParsedSheetById = (id) => {
  try {
    s3.getObject({
      Key: `public/data/sheets/${id}.md`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME
    }, (err, rep) => {
      if (err) throw err
      const data = rep.Body.toString()
      const highlightedCode = hljs.highlight(data, {language: 'caraclog'}).value
      parsedSheets[id] = (new jsdom.JSDOM(highlightedCode)).window.document
      console.log("updateParsedSheetById", id)
    })
  } catch (error) {
    console.error(error)
  }
}

const updateParsedSheets = (req) => {
  if (req?.params?.id) {
    updateParsedSheetById(req?.params?.id)
  }
  else {
    sheetsJSON.forEach(e => {
      updateParsedSheetById(e.id)
    })
  }
}


const formatResponse = (txt) => "```"+txt+"```"
const docTxt = "Utilisation: ![perso] [entrée]" 
const getAvailablePersosTxt = () => `Personnages disponibles: ${sheetsJSON.map(e => e.id).join(', ')}`
const getAvailableEntriesTxt = () => `Entrées disponibles: ${ENTRIES.map(e => e.key).map(e => e === '' ? '[aucune entrée]':e).join(', ')}`

const formatStr = s => s.trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()

const findMatch = (str, array) => {
  if (!str)
    return {valid: false, str}
  for (let e of array) {
    if ((e+'s').startsWith(str)) 
      return {valid: true, str:e}
  }
  return {valid: false, str}
}



const sendPersoInfo = (perso, key, num) => {
  let result = ''
  if (key === 'ressource') {
    const nodes = Array.from(parsedSheets[perso].getElementsByClassName('hljs-ressource'))
    result = nodes.map(n => '\n'+n.textContent).join('')
  }
  else if (key === 'competence') {
    const nodes = Array.from(parsedSheets[perso].getElementsByClassName('hljs-competence'))
    result = nodes.map(n => '\n'+n.textContent).join('')
  }
  else if (key === 'capacite') {
    let index = parseInt(num)
    const capaNodes = Array.from(parsedSheets[perso].getElementsByClassName('hljs-capacity'))
    if (!isNaN(index) && index>=0 && index<capaNodes.length) {
      result = '\n'+capaNodes[index].textContent
    } else {
      const nodes = Array.from(parsedSheets[perso].getElementsByClassName('hljs-capacity-name'))
      result = nodes.map((n, i) => `\n${i}:`+n.textContent.replace('###', '')).join('')
      result += `\nPour plus de détail: !${perso} capacite [numéro]`
    }
  }
  return formatResponse(`${perso}/${key}${num?'/'+num:''}: ${result}`)
}



const onMessage = (message) => { 
  if (message.author.bot) return
  if (!message.content.startsWith(CMD_PREFIX)) return
  const channel = message.channel

  console.log(`[botClient message] from: ${message.author} with content: ${message.content}`)

  const commandBody = message.content.slice(CMD_PREFIX.length)
  const args = commandBody.split(' ').map(s => formatStr(s))
  const cmd = args.shift()
  
  if (['info', 'help'].includes(cmd)) {
    channel.send(formatResponse(docTxt+"\n"+getAvailablePersosTxt()+"\n"+getAvailableEntriesTxt()))
  }
  else if (cmd === 'ping') {
    channel.send(formatResponse(`pong!`))
  }
  else if (cmd) {
    const perso = findMatch(cmd, sheetsJSON.map(e => e.id))
    const entry = findMatch(args?.[0], ENTRIES.map(e => e.key))
    !perso.valid && entry.valid && channel.send(formatResponse(getAvailablePersosTxt()))
    perso.valid && !entry.valid && channel.send(formatResponse(getAvailableEntriesTxt()))
    perso.valid && entry.valid && channel.send(sendPersoInfo(perso.str, entry.str, args?.[1]))
  }
}

module.exports = {
  onMessage,
  updateParsedSheets
}