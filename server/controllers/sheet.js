const AWS = require('aws-sdk')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const getSheets = (req, res) => {
  console.log('GET /sheets')
  try {
    s3.getObject({
      Key:    'public/data/sheets.json',
      Bucket: process.env.BUCKETEER_BUCKET_NAME
    }, (err, data) => {
      if (err) throw err
      res.json(JSON.parse(data.Body.toString()))
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({error})
  }
}

const getSheetById = (req, res) => {
	console.log('GET /sheets/:id', req.params.id)
  try {
    s3.getObject({
      Key: `public/data/sheets/${req.params.id}.md`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME
    }, (err, data) => {
      if (err) throw err
      res.send({
        lastModified: data.LastModified,
        body: data.Body.toString()
      })
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({error})
  }
}

const updateSheet = (req, res, next) => {
	console.log('POST /sheets', req.params.id)
  try {
    if (process.env.NODE_ENV === 'dev') {
      res.status(200).json({stdout:'Dev: pas de sauvegarde', stderr:'RAS'})
      next()
    }
    else {
      s3.putObject({
        Key: `public/data/sheets/${req.params.id}.md`,
        Bucket: process.env.BUCKETEER_BUCKET_NAME,
        Body: Buffer.from(req.body.data)
      }, (err, data) => {
        if (err) throw err
        res.status(200).json({
          stdout:'Ok', 
          stderr:'RAS',
          lastModified: Date.now()
        })
        next()
      })
    }
  } catch (err) {
    res.status(500).send(err)
  }
}

// TODO: sauvegarder sur gitlab
// exec("sh ./scripts/commitPushSheetData.sh "+req.params.id, (error, stdout, stderr) => {
//   if (error) {
//     throw new Error(error.message)
//   }
//   console.warn(`stderr: ${stderr}`)
//   console.log(`stdout: ${stdout}`)
//   res.status(200).json({stdout, stderr})
//   next()
// })

module.exports = { 
  getSheets,
  getSheetById,
  updateSheet
}