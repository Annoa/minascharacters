require('dotenv').config()

const express = require('express')
const path = require('path')
const app = express()
const compression = require('compression')
const cors = require('cors')
const server = require('http').createServer(app)
const { Client } = require('discord.js')

global.appRoot = path.resolve(__dirname)

const loginController = require('./server/controllers/login')
const sheetController = require('./server/controllers/sheet')
const rootController = require('./server/controllers/root')
const botController = require('./server/controllers/bot')

const botClient = new Client({ intents: ["GUILDS", "GUILD_MESSAGES", "DIRECT_MESSAGES"] });

botClient.login(process.env.BOT_TOKEN)


app.use(cors())
app.options('*', cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(compression())

app.use(express.static(path.join(__dirname, 'build')))

botController.updateParsedSheets()

app.post('/login', loginController.login)
app.get('/tokenIsValid', loginController.authenticateToken, loginController.tokenIsValid)
app.get('/sheets', sheetController.getSheets)
app.get('/sheets/:id', sheetController.getSheetById)
app.post('/sheets/:id', loginController.authenticateToken, sheetController.updateSheet, botController.updateParsedSheets)
app.get('/racines', rootController.getRoots)
botClient.on('messageCreate', botController.onMessage)

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
})

const PORT = process.env.PORT || 80

server.listen(PORT, () => {
	console.log(`server running on port ${PORT}`)
})