import React from "react"
import ReactDOM from "react-dom"
import App from "./components/App"
import './styles/variables.css'
import './assets/fonts/fonts.css'
import "./styles/index.css"
import "./styles/app.css"
import "./styles/codeEditor.css"
import "./styles/utils.css"
import "./styles/sheet.css"
import "./styles/snackbar.css"
import "./styles/root.css"
import "@fortawesome/fontawesome-free/js/fontawesome"
import "@fortawesome/fontawesome-free/js/solid"
import "@fortawesome/fontawesome-free/js/regular"
import "@fortawesome/fontawesome-free/js/brands"
import AuthHandler from "./components/AuthHandler"

ReactDOM.render(
  <React.StrictMode>
    <AuthHandler>
      <App />
    </AuthHandler>
  </React.StrictMode>,
  document.getElementById('root')
)
