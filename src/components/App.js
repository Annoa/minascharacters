import { useEffect, useState } from 'react'
import axios from 'axios'
import {
  BrowserRouter as Router,
  NavLink,
  Route,
  Switch,
} from 'react-router-dom'
import { getAppUrl } from '../helpers/app'

import Sheet from './Sheet'
import Snackbar, { useSnackbar } from './Snackbar'
import Roots from './Root'

const localFetch = axios.create({ baseURL: getAppUrl() })

const App = ({
  isAuth,
  AuthButton
}) => {
  const [sheets, setSheets] = useState([])
  const {displaySnackbar, snackbarContent, snackbarError} = useSnackbar()

  useEffect(() => {
    localFetch.get('sheets').then(res => {
      setSheets(res.data)
    }).catch(err => {
      console.error(err)
      displaySnackbar(['GET sheets', err?.message], true)
    })
  }, [displaySnackbar])

  return (
    <Router>
      <div className="App">
        <nav className="App-nav">
          <div className="App-nav-inner">
          { AuthButton }
          {/* <div className="App-nav-center"> */}
          {sheets.map(sheet => 
            <NavLink to={`/sheet/${sheet.id}`} className="nav-button link-to-sheet" 
              activeClassName="nav-button-active"
              key={sheet.id}
            >
              <div>
                <span className="resp-display-sm">{sheet.name[0]}</span>
                <span className="resp-display-lg">{sheet.name}</span>
              </div>
            </NavLink>
          )}
          {/* </div> */}
          <NavLink to="/roots" className="nav-button" 
              activeClassName="nav-button-active"
            >
            <div>Racines</div>
          </NavLink>
          </div>
        </nav>
        <div className="App-content">
          <Switch>
            <Route path="/sheet/:id">
              <Sheet isAuth={isAuth} displaySnackbar={displaySnackbar}/>
            </Route>
            <Route path="/roots">
              <Roots/>
            </Route>
          </Switch>
        </div>
        <Snackbar content={snackbarContent} isError={snackbarError}/>
      </div>
    </Router>
  )
}



export default App