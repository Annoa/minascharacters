import axios from "axios"
import { useCallback, useEffect, useRef, useState } from "react"
import { useParams } from "react-router"
import { getAppUrl } from "../../helpers/app"
import ScrollButton from "../ScrollButon"
import EditSheet from "./EditSheet"
import InfoPanel from "./InfoPanel"
var showdown  = require('showdown')

const localFetch = axios.create({ baseURL: getAppUrl() })

const Sheet = ({
  isAuth,
  displaySnackbar
}) => {
  let { id } = useParams()
  const [sheet, setSheet] = useState()
  const [lastModified, setLastModified] = useState()
  const [html, setHtml] = useState()
  const [editing, setEditing] = useState(false)
  const converter = useRef()
  const sheetDiv = useRef()
  
  const toggleEditing = useCallback((shouldBlockNavigation) => {
    if (editing) {
      if (shouldBlockNavigation) {
        const text = "You have unsaved changes, are you sure you want to leave?"
        if (window.confirm(text) === true) {
          setEditing(false)
        }
      } else {
        setEditing(false)
      }
    } else {
      setEditing(true)
    }
  }, [setEditing, editing])

  const save = useCallback((content) => {
    const config = {headers: { Authorization: `Bearer ${window.localStorage.getItem('minas_charac_token')}` }}
    localFetch.post('sheets/'+id, {
      data: content
    }, config).then(res => {
      const date = new Date(res.data.lastModified)
      const options = {month: 'numeric', day: 'numeric', hour: '2-digit', minute: '2-digit'}
      setLastModified(date.toLocaleString('fr-FR', options))
      displaySnackbar(['Save sheet', ...Object.values(res)])
      setSheet(content)
    }).catch(err => {
      console.error(err)
      displaySnackbar(['Save sheet', err?.message], true)
    })
  }, [id, displaySnackbar])

  useEffect(() => {
    converter.current = new showdown.Converter()
  }, [])

  useEffect(() => {
    if (converter?.current && sheet) {
      setHtml(converter.current.makeHtml(sheet))
    }
  }, [sheet])

  useEffect(() => {
    setSheet('')
    setLastModified('')
    localFetch.get('sheets/'+id).then(res => {
      const date = new Date(res.data.lastModified)
      const options = {month: 'numeric', day: 'numeric', hour: '2-digit', minute: '2-digit'}
      setLastModified(date.toLocaleString('fr-FR', options))
      setSheet(res.data.body)
    }).catch(err => {
      console.error(err)
      displaySnackbar(['GET sheet '+id, err?.message], true)
    })
  }, [id, displaySnackbar])

  if (!sheet)
    return <div>Loading...</div>
  
  return (
    <div className="sheet">
      {!editing ?
        <>
          <div className="sheet-html"> 
            <div ref={sheetDiv} dangerouslySetInnerHTML={{ __html: html }} />
          </div>
          <InfoPanel 
            toggleEditing={toggleEditing} 
            html={html}
            sheetDiv={sheetDiv}
            isAuth={isAuth}
          />
        </>
        :
        <EditSheet
          sheet={sheet}
          lastModified={lastModified}
          save={save}
          toggleEditing={toggleEditing}
        />
      }
      <ScrollButton/>
    </div>
  )
}

export default Sheet