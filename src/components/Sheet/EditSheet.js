import { useCallback, useEffect, useState } from "react"
import useCodeEditor from "./CodeEditor"
import hljs from "highlight.js/lib/core"
import useDebounce from "../../hooks/useDebounce"
import { Prompt } from "react-router"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
const parser = require("../../helpers/parser")

const highlight = (htmlElem) => {
  const code = htmlElem.textContent || ''
  const highlightedCode = hljs.highlight(code, {language: 'caraclog'}).value
  htmlElem.innerHTML = highlightedCode
}

const EditSheet = ({
  sheet,
  save,
  toggleEditing,
  lastModified
}) => {
  const [content, setContent] = useState('')
  const [usedPC, setUsedPc] = useState(0)
  const [usedPX, setUsedPx] = useState(0)
  const [shouldBlockNavigation, setShouldBlockNavigation] = useState(true)
  
  const debouncedContent = useDebounce(content, 1000)

  useEffect(() => {
    hljs.registerLanguage('caraclog', parser.default)
  }, [])

  useEffect(() => {
    if (shouldBlockNavigation) {
      window.onbeforeunload = () => true
    } else {
      window.onbeforeunload = undefined
    }
  }, [shouldBlockNavigation])

  useEffect(() => {
    setContent(sheet)
    setShouldBlockNavigation(false)
  }, [sheet])

  useEffect(() => {
    const handleKeySave = e => {
      const isCtrl = e => e.metaKey || e.ctrlKey
      if (isCtrl(e) && e.key === 's') {
        e.preventDefault()
        shouldBlockNavigation && save(content)
      }
    }
    window.addEventListener('keydown', handleKeySave)
    return () => {
      window.removeEventListener('keydown', handleKeySave)
    }
  }, [save, content, shouldBlockNavigation])

  useEffect(() => {
    if (
      !shouldBlockNavigation &&
      content !== sheet
    ) {
      setShouldBlockNavigation(true)
    } 
  }, [content, sheet, shouldBlockNavigation])

  const { setEditorRefCb, editorRef, handleClick } = useCodeEditor({
    highlightFunc: highlight,
    codeJarOptions: {tab: '  '},
    code: content,
    onUpdate: setContent
  })

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const computePc = useCallback(() => {
    if (!editorRef)
      return
    let usedPC = 0
    const capacitiesElems = Array.from(editorRef.getElementsByClassName('hljs-capacity'))
    capacitiesElems.forEach(e => {
      const pcs = Array.from(e.getElementsByClassName('hljs-creation-point'))
        .map(e => parseInt(e.textContent.slice(0, -2)))
        .reduce((a, b) => a + b, 0)
      let multipliers = Array.from(e.getElementsByClassName('hljs-multiplier-pc'))
        .map(e => parseInt(e.textContent.slice(0, -1))).reduce((a, b) => a * ((b+100)/100), 1)
      let sumSpanArray = Array.from(e.getElementsByClassName('hljs-total-pc'))
      let sumSpan
      let total = Math.round(pcs*multipliers)
      if (total > 0) {
        usedPC += total
      }
      if (sumSpanArray.length) {
        sumSpan = sumSpanArray[0]
        sumSpan.innerHTML = 'Total : '+ total +'pc'
      }
    })
    setUsedPc(usedPC)
  }, [editorRef])

  const computePxForRoots = useCallback(() => {
    let total = 0
    const rootElems = Array.from(editorRef.getElementsByClassName('hljs-root_known'))
    rootElems.forEach(e => {
      let matchs = e.textContent.match(/\((C|U|R|M)\)/)
      if (matchs && matchs.length) {
        if (matchs[1] === 'C') total += 1
        if (matchs[1] === 'U') total += 2
        if (matchs[1] === 'R') total += 3
        if (matchs[1] === 'M') total += 4
      }
    })
    if (editorRef.getElementsByClassName('hljs-roots_known') && editorRef.getElementsByClassName('hljs-roots_known')[0]) {
      let allSkillElems = editorRef.getElementsByClassName('hljs-roots_known')[0].children
      if (allSkillElems[allSkillElems.length - 2].className === 'hljs-total-pc') {
        allSkillElems[allSkillElems.length - 2].innerHTML = 'Total : '+ total +' XP'
      }
    }
    return total
  }, [editorRef])

  const computePxForRessources = useCallback(() => {
    let total = 0
    const rootElems = Array.from(editorRef.getElementsByClassName('hljs-ressource'))
    rootElems.forEach(e => {
      let matchs = e.textContent.match(/\[([0-9]+)\/{0,1}([0-9]*)\]/)
      if (matchs && matchs.length) {
        total += parseInt(matchs[1])
      }
    })
    return total
  }, [editorRef])

  const computePxForSkills = useCallback(() => {
    let total = 0
    const skillElems = Array.from(editorRef.getElementsByClassName('hljs-competence'))
    skillElems.forEach(e => {
      let matchs = e.textContent.match(/\d+/)
      if (matchs && matchs.length) {
        total += parseInt(matchs[0])
      }
    })
    if (editorRef.getElementsByClassName('hljs-competences') && editorRef.getElementsByClassName('hljs-competences')[0]) {
      let allSkillElems = editorRef.getElementsByClassName('hljs-competences')[0].children
      if (allSkillElems[allSkillElems.length - 1].className === 'hljs-total-pc') {
        allSkillElems[allSkillElems.length - 1].innerHTML = 'Total : '+ total +' XP'
      }
    }
    return total
  }, [editorRef])

  const computePxForFeatsOrFeatures = useCallback(className => {
    let total = 0
    const skillElems = Array.from(editorRef.getElementsByClassName(className))
    skillElems.forEach(e => {
      let matchs = e.textContent.match(/\((\d+)( |)(PX|px|Px|PXs|PXS|XP|Xp|XPS|XPs)\)/)
      if (matchs && matchs.length) {
        total += parseInt(matchs[1])
      }
    })
    return total
  }, [editorRef])

  const computePx = useCallback(() => {
    if (!editorRef)
      return

    let grantTotal = 0
    grantTotal += computePxForSkills()
    grantTotal += computePxForRoots()
    grantTotal += computePxForRessources()
    grantTotal += computePxForFeatsOrFeatures('hljs-feat')
    grantTotal += computePxForFeatsOrFeatures('hljs-feature')
    setUsedPx(grantTotal)
  }, [
    editorRef, 
    setUsedPx, 
    computePxForSkills,
    computePxForRoots,
    computePxForRessources,
    computePxForFeatsOrFeatures
  ])

  useEffect(() => {
    if (!editorRef)
      return
    computePc()
    computePx()
  }, [
    debouncedContent, 
    editorRef, 
    computePc, 
    computePx
  ])

  const handleSave = useCallback(() => {
    save(content)
  }, [save, content])
  
  return (
    <>
      <Prompt
        when={shouldBlockNavigation}
        message="You have unsaved changes, are you sure you want to leave?"
      />

      <div className="code-edit-control-bar flex">
        <div className="flex-grow-1 flex flex-wrap">
          <div className="flex-grow-1">{usedPX + Math.ceil(usedPC/10)} XP reste {Math.ceil((Math.ceil(usedPC/10)-usedPC/10)*10)} PC ({usedPX} XP dépensés + {usedPC} PC dépensés)</div>
          <div>{lastModified}</div>
        </div>
        <button disabled={!shouldBlockNavigation} onClick={handleSave}>
          <FontAwesomeIcon icon={["fas", "save"]} size="lg"/>
        </button>
        <button onClick={() => toggleEditing(shouldBlockNavigation)}>
          <FontAwesomeIcon icon={["fas", "window-close"]} size="lg"/>
        </button>
      </div>
      
      <div 
        className="code-edit-container" 
        onClick={handleClick} 
        ref={setEditorRefCb}
        />
    </>
  )
}

export default EditSheet