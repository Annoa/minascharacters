/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useCallback, useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"


const InfoPanel = ({
  toggleEditing,
  isAuth,
  html,
  sheetDiv
}) => {
  const [headers, setHeaders] = useState([])
  const [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    if (sheetDiv.current) {
      setHeaders(Array.from(sheetDiv.current.querySelectorAll("h1, h2, h3")))
    }
  }, [sheetDiv, html]) 

  useEffect(() => {
    window.addEventListener('resize', e => {
      setIsOpen(false)
    })
  }, [setIsOpen])

  const onArrowClick = useCallback(() => {
    setIsOpen(!isOpen)
  }, [setIsOpen, isOpen])

  const stylePanel = isOpen? {transform:'translate(0,0)'}:{}
  const styleButton = !isOpen? {transform:'translate(0,0)'}:{}
  
  return (
    <>
      <div className="info-panel" style={stylePanel}>
        <button
          disabled={!isAuth}
          onClick={toggleEditing}
        >
          <FontAwesomeIcon icon={["fas", "edit"]} /> Editer 
        </button>
        { headers.map((h,i) => <a key={'summary'+h.id} href="#" onClick={e => {
          e.preventDefault()
          h.scrollIntoView()
        }}>
          {React.createElement(
            h.localName,
            {className: 'greeting'},
            h.textContent
          )}
          </a>
        )}
      </div>

      <div className="info-panel-button" 
        style={styleButton}
        onClick={onArrowClick}
      >
        <FontAwesomeIcon icon={["fas", isOpen?'arrow-right':'arrow-left']} />
      </div>
    </>
  )
}

export default InfoPanel