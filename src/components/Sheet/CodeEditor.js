import { CodeJar } from "../../codejar/codejar"
import { useCallback, useEffect, useRef, useState } from "react"
import { getCaretOffset, setCurrentCursorPosition } from "../../helpers/caret"


// type CodejarOptions = {
//   tab: string
//   indentOn: RegExp
//   spellcheck: boolean
//   catchTab: boolean
//   preserveIdent: boolean
//   history: boolean
//   window: Window
//   addClosing: boolean
// }

const useCodeEditor = ({
  highlightFunc,
  codeJarOptions,
  code,
  onUpdate,
}) => {
  const jar = useRef(null)
  const [editorRef, setEditorRef] = useState(null)
  const [cursorOffset, setCursorOffset] = useState(0)

  const setEditorRefCb = useCallback(node => {
    setEditorRef(node)
  }, [])

  const handleClick = useCallback(() => {
    if (!editorRef) return
    setCursorOffset(getCaretOffset(editorRef))
  }, [editorRef])

  useEffect(() => {
    if (!editorRef) return
    jar.current = CodeJar(editorRef, highlightFunc, codeJarOptions)
    jar.current.onUpdate(txt => {
      if (!editorRef) return
      setCursorOffset(getCaretOffset(editorRef))
      onUpdate(txt)
    })
    return () => jar.current?.destroy()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [editorRef])

  useEffect(() => {
    if (!jar.current || !editorRef) return
    jar.current.updateCode(code)
    setCurrentCursorPosition(editorRef, cursorOffset)
     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [code])

  useEffect(() => {
    if (!jar.current || !codeJarOptions) return
    jar.current.updateOptions(codeJarOptions)
  }, [codeJarOptions])

  return { setEditorRefCb, editorRef, handleClick }
}

export default useCodeEditor
