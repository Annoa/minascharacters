/* eslint-disable jsx-a11y/anchor-is-valid */
const Tags = ({
  generique,
  rarete,
  endurance,
  mana,
  volonte,
  factorisable,
  cumulative,
  instant,
  other
}) => {
  return <>
    {generique && <div className="root-label">Générique</div>}
    {rarete && <div className="root-label">{rarete}</div>}
    {endurance && <div className="root-label">
      Endurance{endurance!=='0'?':'+endurance:''}
    </div>}
    {mana && <div className="root-label">
      Mana{mana!=='0'?':'+mana:''}
    </div>}
    {volonte && <div className="root-label">
      Volonté{volonte!=='0'?':'+volonte:''}
    </div>}
    {factorisable && <div className="root-label">Factorisable</div>}
    {instant && <div className="root-label">Instantané</div>}
    {cumulative && <div className="root-label">Cumulatif</div>}
    {other && <div className="root-label">{other}</div>}
  </>
}

const Root = ({
  name,
  desc,
  branch,
  ...props
}) => {
  return (
    <div className="root">
      <div className="root-header">
        <h2 className="root-name">
          <a id={name}>{name}</a>
        </h2>
        <Tags {...props}/>
      </div>

      <div className="root-body">
        <div className="branch-desc" 
          dangerouslySetInnerHTML={{ __html: desc }}
        />
        
        { branch.length > 0 &&
          // <div className={`root-branches grid-${branch?.length%2?'3':'2'}`}>
          <div className={`root-branches grid-2`}>
            {branch.map(b => <div key={b.name}>
              <div className="branch-header">
                <h3 className="branch-name">{b.name}</h3>
                <Tags {...b}/>
              </div>
              <div className="branch-desc" 
                dangerouslySetInnerHTML={{ __html: b.desc }}
              />
            </div>)}
          </div>
        }
      </div>
    </div>
  )
}

export default Root