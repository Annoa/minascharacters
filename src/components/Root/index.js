import { useCallback, useEffect, useState } from 'react'
import axios from 'axios'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import { getAppUrl } from '../../helpers/app'

import Snackbar, {useSnackbar} from '../Snackbar'
import Root from './Root'
import ScrollButton from '../ScrollButon'


const localFetch = axios.create({ baseURL: getAppUrl() })

const normalize = (str) => {
  let res = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
  res = res.toLowerCase()
  return res
} 


const Roots = () => {
  const [racines, setRacines] = useState([])
  const [filtredRacine, setFiltredRacines] = useState([])
  const [filter, setFilter] = useState({
    name:'',
    rarete : []
  })

  const {displaySnackbar, snackbarContent, snackbarError} = useSnackbar()

  const onInputChange = useCallback(e => {
    const id = e.target.id
    if (id === 'name') {
      setFilter({...filter, name:normalize(e.target.value)})
    } else if (['endurance', 'mana', 'volonte', 'generique', 'factorisable', 'cumulative', 'instant'].includes(id)) {
      setFilter({...filter, [id]: e.target.checked})
    } if (e.target.name === 'rarete') {
      if (e.target.checked) {
        setFilter({...filter, rarete: [...filter.rarete, e.target.id]})
      } else {
        setFilter({...filter, rarete: filter.rarete.filter(f =>
          f !== e.target.id
        )})
      }
    }
  }, [setFilter, filter])

  useEffect(() => {
    let res = racines

    if (filter.name) {
      res = res.filter(r => {
        return normalize(r.name).includes(filter.name) || r.branch.find(b => normalize(b.name).includes(filter.name))
      })
    }
    ['endurance', 'mana', 'volonte', 'generique', 'factorisable', 'cumulative', 'instant'].forEach(val => {
      if (filter[val]) {
        res = res.filter(r => r[val] || r.branch.find(b => b[val]))
      }
    })
    if (filter.rarete.length) {
      res = res.filter(r => filter.rarete.includes(r.rarete))
    }
    setFiltredRacines(res)
  }, [racines, filter])

  useEffect(() => {
    localFetch.get('racines').then(res => {
      console.log('fetch racines', res)
      // const converter = new showdown.Converter()
      // converter.setOption('simpleLineBreaks', true)
      // for (const root of res.data) {
      //   console.log('bisou', root)
      //   res.desc = root.desc && converter.makeHtml(root.desc.join('\n'))
      //   console.log('bisou2', root.desc)
      // }
      setRacines(res.data.sort((a,b)=>
        a.name.localeCompare(b.name)
      ))
    }).catch(err => {
      console.error(err)
      displaySnackbar(['Echec de l\'authentification', err?.message], true)
    })
  }, [displaySnackbar])

  return (
    <div className="flex">
      <div className="flex-grow-1">
        <div className="flex-column root-filter-bar">

          <div className="root-filter-bar-input">
            <input id="name" type="text" onChange={onInputChange}/>
            <FontAwesomeIcon icon={["fas", "search"]}/>
          </div>

          <div className="flex justifiy-evenly flex-wrap">

            <div className="flex-column align-start">
              {/* <span>Ressources : </span> */}
              <div className="no-wrap">
                <input id="endurance" type="checkbox" onChange={onInputChange}/>
                <label htmlFor="endurance"> Endurance </label>
              </div>
              <div className="no-wrap">
                <input id="mana" type="checkbox" onChange={onInputChange}/>
                <label htmlFor="mana"> Mana </label>
              </div>
              <div className="no-wrap">
                <input id="volonte" type="checkbox" onChange={onInputChange}/>
                <label htmlFor="volonte"> Volonté </label>
              </div>
            </div>
            
            <div className="flex">
              {/* <div>Rareté : </div> */}
              <div className="flex-column align-start">
                <div className="no-wrap">
                  <input name="rarete" id="C" type="checkbox" onChange={onInputChange}/>
                  <label htmlFor="C"> C </label>
                </div>
                <div className="no-wrap">
                  <input name="rarete" id="U" type="checkbox" onChange={onInputChange}/>
                  <label htmlFor="U"> U </label>
                </div>
              </div>
              <div className="flex-column align-start">
                <div className="no-wrap">
                  <input name="rarete" id="R" type="checkbox" onChange={onInputChange}/>
                  <label htmlFor="R"> R </label>
                </div>
                <div className="no-wrap">
                  <input name="rarete" id="M" type="checkbox" onChange={onInputChange}/>
                  <label htmlFor="M"> M </label>
                </div>
              </div>
            </ div>

            <div className="flex-column align-start">
              <div className="no-wrap">
                <input id="generique" type="checkbox" onChange={onInputChange}/>
                <label htmlFor="generique"> Branche générique </label>
              </div>

              <div className="no-wrap">
                <input id="instant" type="checkbox" onChange={onInputChange}/>
                <label htmlFor="instant"> Instantané </label>
              </div>
            </div>

            <div className="flex-column align-start">
              <div className="no-wrap">
                <input id="cumulative" type="checkbox" onChange={onInputChange}/>
                <label htmlFor="cumulative"> Cumulatif </label>
              </div>

              <div className="no-wrap">
                <input id="factorisable" type="checkbox" onChange={onInputChange}/>
                <label htmlFor="factorisable"> Factorisable </label>
              </div>
            </div>
          </div>

          
        </div>

        
        
        <div className="roots-container">
          <div>
          {filtredRacine.map(racine => <a 
          className='link-to-racine'
              key={racine.name}
              href={'#'+racine.name}
            >
              {racine.name}
          </a>)}
          </div>
          {filtredRacine.map(racine => <Root 
            key={racine.name}
            {...racine}
          />)}
        </div>
      </div>
      <Snackbar content={snackbarContent} isError={snackbarError}/>
      <ScrollButton/>
    </div>
  )
}

export default Roots