import { useCallback, useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const ScrollButton = () => {
  const [isHidden, setIsHidden] = useState(true)

  const onClick = useCallback(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  useEffect(() => {
    window.addEventListener('scroll', e => {
      setIsHidden(window.scrollY < 500)
    })
  }, [setIsHidden])

  return (
    <div className={`scroll-button ${isHidden?'d-none':''}`} onClick={onClick}>
      <FontAwesomeIcon icon={["fas", "arrow-up"]} />
    </div>
  )
}

export default ScrollButton