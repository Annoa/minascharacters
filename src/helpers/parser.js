/*
https://github.com/highlightjs/highlight.js/blob/main/src/languages/markdown.js

https://highlightjs.readthedocs.io/en/latest/language-guide.html

*/

function parser(hljs) {
  const INLINE_HTML = {
    begin: /<\/?[A-Za-z_]/,
    end: '>',
    subLanguage: 'xml',
    relevance: 0
  }
  const HORIZONTAL_RULE = {
    begin: '^[-\\*]{3,}',
    end: '$'
  }
  const LIST = {
    className: 'bullet',
    begin: '^[ \t]*([*+-]|(\\d+\\.))(?=\\s+)',
    end: '\\s+',
    excludeEnd: true
  }
  const BOLD = {
    className: 'strong',
    contains: [], // defined later
    variants: [
      {
        begin: /\[(?!\])/,
        end: /\]/
      }
    ]
  }
  const ITALIC = {
    className: 'emphasis',
    contains: [], // defined later
    variants: [
      {
        begin: /\((?!\))/,
        end: /\)/
      }
    ]
  }
  const CREATION_POINT = {
    className: 'creation-point',
    match: /(-|)[0-9]+( |)pc/
  }
  const MULTIPLIER_PC = {
    className: 'multiplier-pc',
    match: /(-|)[0-9]+( |)%/
  }
  const EXP_POINT = {
    className: 'exp-point',
    match: /[0-9]+( |)(xp|XP|px|PX)(s|S|)/
  }
  BOLD.contains.push(ITALIC)
  ITALIC.contains.push(BOLD)

  let CONTAINABLE = [
    INLINE_HTML,
    EXP_POINT,
  ]

  BOLD.contains = BOLD.contains.concat(CONTAINABLE)
  ITALIC.contains = ITALIC.contains.concat(CONTAINABLE)

  const GRAND_TOTAL_PC = {
    className: 'grand-total-pc',
    begin: '^[*+-]{1}.*PC',
    end: '$',
  }

  CONTAINABLE = CONTAINABLE.concat(BOLD, ITALIC)

  const BRANCH = {
    className: 'branch',
    begin: '^[ \t]+[*+-]{1}',
    end: '$',
    contains: CONTAINABLE.concat(MULTIPLIER_PC, CREATION_POINT)
  }

  const ROOT = {
    className: 'root',
    begin: '^[*+-]{1}( |)',
    end: '$',
    excludeBegin: true,
    contains: CONTAINABLE
  }

  const TOTAL_PC = {
    className: 'total-pc',
    begin: '^( |)(T|t)otal',
    end: '$',
  }

  const RESSOURCE = {
    className: 'ressource',
    begin: '^[*+-]{0,1}.*:( |)[0-9]+\/{0,1}[0-9]*( |)\[[0-9]+\/{0,1}[0-9]*\]( |)',
    end: '$',
    endsWithParent: true,
    returnEnd: true,
  }

  const RESSOURCES = {
    className: 'ressources',
    begin: '^#{2}( |)Ressources',
    beginScope: 'ressources-name',
    end: '^#{2}[^#]',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE.concat(RESSOURCE),
  }

  const COMPETENCE = {
    className: 'competence',
    begin: '^[*+-]{1}.*:( |)[0-9]+.*',
    end: '$',
    endsWithParent: true,
    returnEnd: true,
  }

  const COMPETENCES = {
    className: 'competences',
    begin: '^#{2}( |)Compétences',
    beginScope: 'competences-name',
    end: '^#{2}[^#]',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE.concat(COMPETENCE, TOTAL_PC),
  }

  const ROOT_KNOWN = {
    className: 'root_known',
    begin: '(^|,)(?!(T|t)otal)',
    end: '(,|(T|t)otal)',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE
  }

  const ROOTS_KNOWN = {
    className: 'roots_known',
    begin: '^#{2}( |)Racines.*$',
    beginScope: 'roots-known-name',
    end: '^#{2}[^#]',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE.concat(ROOT_KNOWN, TOTAL_PC),
  }

  const FEAT = {
    className: 'feat',
    begin: '(^|,)',
    end: '(\\(|,)',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE
  }

  const FEATS = {
    className: 'feats',
    begin: '^#{2}( |)Dons',
    beginScope: 'feats-name',
    end: '^#{2}[^#]',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE.concat(FEAT),
  }

  const FEATURE = {
    className: 'feature',
    begin: '(^|,)',
    end: '(\\(|,)',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE
  }

  const FEATURES = {
    className: 'features',
    begin: '^#{2}( |)(t|T)rait.*$',
    beginScope: 'features-name',
    end: '^#{2}[^#]',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE.concat(FEATURE),
  }

  const CAPACITY = {
    className: 'capacity',
    begin: ['^#{3}( |).*', '$'],
    beginScope: {
      1: 'capacity-name'
    },
    end:'^#{3}[^#]',
    endsWithParent: true,
    returnEnd: true,
    contains: CONTAINABLE.concat(ROOT, BRANCH, TOTAL_PC),
  }

  const CAPACITIES = {
    className: 'capacities',
    begin: '^#{2}( |)Capacités',
    beginScope: 'section',
    end: '^#{2}[^#]',
    contains: CONTAINABLE.concat(CAPACITY),
  }

  const CHARACTER_NAME = {
    className: 'character-name',
    variants: [
      {
        begin: '^#{1}',
        end: '$',
        contains: CONTAINABLE
      }
    ]
  }

  const HEADER = {
    className: 'section',
    variants: [
      {
        begin: '^#{2}',
        end: '$',
        contains: CONTAINABLE
      }
    ]
  }

  return {
    name: 'CaracLog',
    aliases: [
      'cl',
      'charlog',
    ],
    contains: [
      GRAND_TOTAL_PC,
      RESSOURCES,
      ROOTS_KNOWN,
      COMPETENCES,
      FEATS,
      FEATURES,
      CAPACITIES,
      HEADER,
      CHARACTER_NAME,
      LIST,
      BOLD,
      ITALIC,
      HORIZONTAL_RULE,
      EXP_POINT
    ]
  }
}

module.exports.default = parser
//export default parser