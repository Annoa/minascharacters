export const getAppUrl = () => {
  return process.env.REACT_APP_BACKEND || (window.location.origin+'/')
}