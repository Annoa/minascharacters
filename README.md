
# Minas Characters

## dev mode

```
npm install
npm run dev-server
npm run start
```

* server on http://localhost:9000 (nodemon)
* client on http://localhost:3000 (create react app)


## prod with heroku

To create
```
heroku login
heroku create minas-charac
heroku config:set PASSWORD=...
heroku config:set SECRET_TOKEN=...
heroku config:set BOT_TOKEN=...
```

To deploy
```
heroku login
git push heroku master
#1 = nb dynamo (free)
heroku ps:scale minas-charac=1
heroku open
```

View logs
```
heroku logs --app=minas-charac  --tail
```

Run locally
```
heroku local web
```

Start a console inside dynamo
```
heroku run bash
```

* /!\ Horoku use env variable $PORT
* Docs : [https://devcenter.heroku.com/articles/getting-started-with-nodejs?singlepage=true#view-logs]


## SVG

https://fontawesome.com

## Bucketeer

https://devcenter.heroku.com/articles/bucketeer

```
$ aws configure
AWS Access Key ID [None]: <BUCKETEER_AWS_ACCESS_KEY_ID>
AWS Secret Access Key [None]: <BUCKETEER_AWS_SECRET_ACCESS_KEY>
Default region name [None]: us-east-1
Default output format [None]:
```